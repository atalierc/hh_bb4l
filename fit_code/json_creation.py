import json

with open('txt_file_name.txt') as file, open('output_txt_file_name.json', 'w') as json_file:
    items = []
    dict1={}
    name="pt_eff"
    for line in file:
        if not line.strip():
            continue
        d = {}
        data = line.split('|')
        for val in data:
            key, sep, value = val.partition(':')
            d[key.strip()] = value.strip()
        items.append(d)
        dict1[name]=items
    json.dump(dict1, json_file)
    
print(dict1)
