import os

limits = [
    [20, 30],
    [30, 40],
#    [40, 50],
#    [50, 60],
#    [60, 100]
]

input_file = 'output_numEvent84000.root'
input_histo = 'histo'
fit_mode = 'voit_exp_correct_fit'
data_txt = 'data_file'
DY_txt = 'DY_file'
txt_name = 'txt_voit_exp_correct_fit'

DY = True
Data = False

for i in range(len(limits)):
    print str(limits[i][0])
    print str(limits[i][1])

    if(Data is True):
        os.system("cat combined_fit_promptMu_data.C | sed 's?txt_txt?"+str(data_txt)+"?g' | sed 's?txt_file_name?"+str(txt_name)+"_data_promptID?g' |  sed 's?input_file_name?"+str(input_file)+"?g' | sed 's?histo_name?"+str(input_histo)+"_data_promptID?g'  | sed 's?canvas2_name?"+str(fit_mode)+"_"+str(limits[i][0])+"_"+str(limits[i][1])+"_data_promptID_pass?g' | sed 's?canvas3_name?"+str(fit_mode)+"_"+str(limits[i][0])+"_"+str(limits[i][1])+"_data_promptID_fail?g' | sed 's?min?"+str(limits[i][0])+"?g' | sed 's?max?"+str(limits[i][1])+"?g' > roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")
        os.system("root -l -q  roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")
        os.system("rm roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")

    if(DY is True):
        os.system("cat combined_fit_promptMu_mc.C | sed 's?txt_txt?"+str(DY_txt)+"?g' | sed 's?txt_file_name?"+str(txt_name)+"_DY?g' |  sed 's?input_file_name?"+str(input_file)+"?g' | sed 's?histo_name?"+str(input_histo)+"_DY?g'  | sed 's?canvas2_name?"+str(fit_mode)+"_"+str(limits[i][0])+"_"+str(limits[i][1])+"_DY_pass?g' | sed 's?canvas3_name?"+str(fit_mode)+"_"+str(limits[i][0])+"_"+str(limits[i][1])+"_DY_fail?g' | sed 's?min?"+str(limits[i][0])+"?g' | sed 's?max?"+str(limits[i][1])+"?g' > roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")
        os.system("root -l -q  roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")
        os.system("rm roofit_loop_"+str(limits[i][0])+"_"+str(limits[i][1])+".C")

if(DY is True):
    os.system("cat json_creation.py | sed 's?txt_file_name?"+str(txt_name)+"_DY?g' > json_creation_modified.py")
    os.system("python json_creation_modified.py")
    os.system("rm json_creation_modified.py")

if(Data is True):
    os.system("cat json_creation.py | sed 's?txt_file_name?"+str(txt_name)+"_data_promptID?g' > json_creation_modified.py")
    os.system("python json_creation_modified.py")
    os.system("rm json_creation_modified.py")
