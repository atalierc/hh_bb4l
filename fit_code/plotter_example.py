import numpy as np
import json
import numpy as np

from matplotlib import pyplot as plt
from matplotlib import rcParams
from matplotlib import pyplot as plt
import matplotlib.ticker as ticker


plt.rcParams.update({'font.size': 20})

fig = plt.figure(figsize=(10,10))
ax  = fig.add_subplot(1,1,1)

# a is an axes object, e.g. from figure.get_axes()

# Hide major tick labels
ax.xaxis.set_major_formatter(ticker.NullFormatter())

ax.xaxis.set_minor_locator(ticker.FixedLocator([0.5,1.5,2.5,3.5,4.5]))
ax.xaxis.set_minor_formatter(ticker.FixedFormatter(["20 30", "30 40", "40 50", "50 60", "60 100"]))

num = 0

limits_files = {'2016': 'output_txt_voit_exp_correct_fit_DY.json'}

for year, filename in limits_files.items():
     with open(filename) as json_file:
        json_dict = json.load(json_file)
        for key in json_dict['pt_eff']:
             print key['efficiency']
             ax.plot(num+0.5,key['efficiency'], 'o', label='MC efficiency', color='red')
             num+=1

ax.set_xlim(0, 5)
ax.set_ylim(0, 1)
ax.set_xticks(ticks=[])
plt.ylabel('Efficiency')
plt.xlabel('GeV')

#ax.set_title("137 $fb^{-1}$ (13 TeV)", loc="right", fontdict={'fontsize': "18", 'fontweight':'normal'})


## Customize legend, otherwise you will have and entry for each of the elements on which you loop
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

custom_lines = [Line2D([0], [0], color="red", lw=1, ls="-", label='MC efficiency')]

ax.legend(handles=custom_lines, loc='lower right', fontsize=16)
ax.tick_params(direction='out', length=6, width=1, colors='k')
ax.tick_params(direction='out', length=3, width=1, colors='k')

plt.savefig("MC_eff_plot.pdf")
