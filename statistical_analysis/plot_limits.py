### THIS MACRO WORKS IN CMSSW_9_4_13
import numpy as np
import json
import numpy as np

from matplotlib import pyplot as plt
from matplotlib import rcParams
from matplotlib import pyplot as plt
import matplotlib.ticker as ticker


plt.rcParams.update({'font.size': 20})

fig = plt.figure(figsize=(10,10))
ax  = fig.add_subplot(1,1,1)

# a is an axes object, e.g. from figure.get_axes()

# Hide major tick labels
ax.xaxis.set_major_formatter(ticker.NullFormatter())

# Customize minor tick labels
#ax.yaxis.set_major_locator(ticker.FixedLocator(range(0,100,10)))
#ax.yaxis.set_major_formatter(ticker.FixedFormatter(range(0,100,10)))

ax.xaxis.set_minor_locator(ticker.FixedLocator([0.5,1.5,2.5,3.5]))
ax.xaxis.set_minor_formatter(ticker.FixedFormatter(["2016", "2017", "2018", "Combined"]))

num = 0

kl_pexp2 = [39.6465, 36.8457, 25.0400]
kl_pexp1 = [58.9321, 53.6566, 36.5396]
kl_central = [95.7500, 85.7500, 57.7500]
kl_mexp1 = [162.1595, 141.1221, 94.5810]
kl_mexp2 = [261.9347, 224.3031, 149.0219]
kl_obs = [82.2979, 106.2727, 42.0009]



for i in range(len(kl_pexp2)):
        print kl_central[i]
        ax.fill_between([num+0.01,num+0.99],kl_mexp2[i], kl_pexp2[i], color="gold", label=r"2 sigma")
        ax.fill_between([num+0.01,num+0.99],kl_mexp1[i], kl_pexp1[i], color="forestgreen", label=r"1 sigma")
        ax.hlines( kl_central[i], num+0.01, num+0.99, colors='k', linestyles='--', lw=0.8,label='Expected Limit')
        ax.vlines( num, 0, 500, colors='k', linestyles='--', linewidth=1)
        ax.plot(num+0.5,kl_obs[i], 'o', label='Obverved Limit', color='k')
        ax.hlines( kl_obs[i], num+0.01, num+0.99, colors='k',label='Observed Limit')

        num+=1

ax.set_xlim(0,4)
ax.set_ylim(8, 400)
ax.set_ylabel(r" $ \sigma / \sigma_{SM}$", rotation=90, fontsize="23")
ax.set_xticks(ticks=[])
#ax.set_title("CMS Preliminary", loc="left", fontdict={'fontsize': "12", 'fontweight':'bold'})

fig.text(.22, 0.85, "CMS", fontweight='bold',
                fontsize=23, color='k', rotation=0,
                ha='right', va='top', alpha=1)
#0.369
fig.text(.4, 0.848, "Preliminary", fontstyle='italic',
                fontsize=21, color='k', rotation=0,
                ha='right', va='top', alpha=1)
ax.set_title("137 $fb^{-1}$ (13 TeV)", loc="right", fontdict={'fontsize': "18", 'fontweight':'normal'})

#plt.title(r"$HH \to bbZZ \to bb4l$", fontdict={'fontsize': "16",'fontweight':'normal'})
plt.yscale("log")

## Customize legend, otherwise you will have and entry for each of the elements on which you loop
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

custom_lines = [Line2D([0], [0], color="black", lw=1, ls="-", label='Observed 95% CL Limit'),
                Line2D([0], [0], color="black", lw=1, ls="--", label='Expected 95% CL Limit'),
                Patch(facecolor="forestgreen", edgecolor='k',label=r'Expected $\pm$1 s.d.'),
                Patch(facecolor="gold", edgecolor='k', label=r'Expected $\pm$2 s.d.')]

ax.legend(handles=custom_lines, fontsize=16)


plt.savefig("SM_bb4l_Limits.pdf")
