import os

list_txt=[
'input_4e'
]

year_2016 = False
year_2017 = False
year_2018 = True

for i in xrange(len(list_txt)):
  if(year_2016 is False and year_2017 is False and year_2018 is False):
    print("Please choose one year")
    break

  if(year_2018):    
    if('4mu' in list_txt[i]): 
      which_channel = '4mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(1.60)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('4e' in list_txt[i]): 
      which_channel = '4e'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(0.72)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('2e2mu' in list_txt[i]):
      which_channel = '2e2mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(2.58)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")

  if(year_2017):
    if('4mu' in list_txt[i]):
      which_channel = '4mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(1.48)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('4e' in list_txt[i]):
      which_channel = '4e'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(0.52)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('2e2mu' in list_txt[i]):
      which_channel = '2e2mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(2.00)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


  if(year_2016):
    if('4mu' in list_txt[i]):
      which_channel = '4mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(0.79)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('4e' in list_txt[i]):
      which_channel = '4e'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(1.40)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")


    if('2e2mu' in list_txt[i]):
      which_channel = '2e2mu'
      os.system("cat create_cards.C | sed 's?input_file?"+list_txt[i]+"?g' | sed 's?f_weight?f_weightsignal_nominal?g' | sed 's?channel?"+which_channel+"?g' | sed 's?scale_ZX?"+str(2.64)+"?g' > update_"+list_txt[i]+".C")
      os.system("root -l -b -q update_"+list_txt[i]+".C")
      os.system("rm update_"+list_txt[i]+".C")
