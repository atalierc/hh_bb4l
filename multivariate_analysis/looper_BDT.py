import os

list_txt=[
'input_4mu'
  ]


for i in xrange(len(list_txt)):

    if('4mu' in list_txt[i]):
        os.system("cat AddBranches_4mu.C | sed 's?input_file?"+list_txt[i]+"?g' > AddBranches_4mu_"+list_txt[i]+".C")
        os.system("root -l -b -q AddBranches_4mu_"+list_txt[i]+".C")
        os.system("rm AddBranches_4mu_"+list_txt[i]+".C")
    
    if('4e' in list_txt[i]):
        os.system("cat AddBranches_4e.C | sed 's?input_file?"+list_txt[i]+"?g' > AddBranches_4e_"+list_txt[i]+".C")
        os.system("root -l -b -q AddBranches_4e_"+list_txt[i]+".C")
        os.system("rm AddBranches_4e_"+list_txt[i]+".C")
    
    if('2e2mu' in list_txt[i]):
        os.system("cat AddBranches_2e2mu.C | sed 's?input_file?"+list_txt[i]+"?g' > AddBranches_2e2mu_"+list_txt[i]+".C")
        os.system("root -l -b -q AddBranches_2e2mu_"+list_txt[i]+".C")
        os.system("rm AddBranches_2e2mu_"+list_txt[i]+".C")
