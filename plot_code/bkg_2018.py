from ROOT import *
from array import array

class Histo:
    def __init__(self, histo_name, color):
        self.histo=TH1D(histo_name,histo_name, 120, 80, 200)
        self.histo.SetFillColor(color)

c_ggH = Histo('H+VBF', kViolet+6)
c_ggZZ = Histo('ggZZ', kAzure-3)
c_qqZZ = Histo('qqZZ', kAzure+6)
c_ttV = Histo('ttV', kBlue+3)
c_others = Histo('others', kGreen -3)
c_ZX = Histo('ZX', kGreen+4)
c_Wjets = Histo('Wjets', kPink)


files={
    'ggH4mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggH125.root', c_ggH, '4mu'], 
    'ZZ2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo2e2mu_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'ZZ2e2tau' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo2e2tau_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'ZZ2mu2tau' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo2mu2tau_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'ZZ4e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo4e_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'ZZ4mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo4mu_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'ZZ4tau' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ggTo4tau_Contin_MCFM701.root', c_ggZZ, '4mu'],
    'VBF_HZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_VBFH125.root', c_ggH, '4mu'],
    'Wminus_HZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_WminusH125.root', c_ggH, '4mu'],
    'Wplus_HZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_WplusH125.root', c_ggH, '4mu'],
    'ZH_HZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ZH125.root', c_ggH, '4mu'],
    'ttH' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ttH125.root', c_ggH, '4mu'],
    'ZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ZZTo4lext2.root', c_qqZZ, '4mu'],
    'ZZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ZZZ.root', c_others, '4mu'],
    'WWZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_WWZ.root', c_others, '4mu'],
    'WZZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_WZZ.root', c_others, '4mu'],
    'ttW' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_TTWJetsToLNu.root', c_ttV, '4mu'],
    'ttZ' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_TTZToLLNuNu_M10.root', c_ttV, '4mu'],
    'ZX' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4mu/reduced_ZXbkg_4ljjsel.root', c_ZX, '4mu'],


    'ggH4mu_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggH125.root', c_ggH, '4e'], 
    'ZZ2e2mu_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo2e2mu_Contin_MCFM701.root', c_ggZZ, '4e'],
    'ZZ2e2tau_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo2e2tau_Contin_MCFM701.root', c_ggZZ, '4e'],
    'ZZ2mu2tau_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo2mu2tau_Contin_MCFM701.root', c_ggZZ, '4e'],
    'ZZ4e_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo4e_Contin_MCFM701.root', c_ggZZ, '4e'],
    'ZZ4mu_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo4mu_Contin_MCFM701.root', c_ggZZ, '4e'],
    'ZZ4tau_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ggTo4tau_Contin_MCFM701.root', c_ggZZ, '4e'],
    'VBF_HZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_VBFH125.root', c_ggH, '4e'],
    'Wminus_HZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_WminusH125.root', c_ggH, '4e'],
    'Wplus_HZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_WplusH125.root', c_ggH, '4e'],
    'ZH_HZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ZH125.root', c_ggH, '4e'],
    'ttH_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ttH125.root', c_ggH, '4e'],
    'ZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ZZTo4lext2.root', c_qqZZ, '4e'],
    'ZZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ZZZ.root', c_others, '4e'],
    'WWZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_WWZ.root', c_others, '4e'],
    'WZZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_WZZ.root', c_others, '4e'],
    'ttW_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_TTWJetsToLNu.root', c_ttV, '4e'],
    'ttZ_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_TTZToLLNuNu_M10.root', c_ttV, '4e'],
    'ZX_e' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs4e/reduced_ZXbkg_4ljjsel.root', c_ZX, '4e'],


    'ggH4mu_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggH125.root', c_ggH, '2e2mu'],   
    'ZZ2e2mu_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo2e2mu_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'ZZ2e2tau_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo2e2tau_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'ZZ2mu2tau_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo2mu2tau_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'ZZ4e_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo4e_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'ZZ4mu_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo4mu_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'ZZ4tau_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ggTo4tau_Contin_MCFM701.root', c_ggZZ, '2e2mu'],
    'VBF_HZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_VBFH125.root', c_ggH, '2e2mu'],
    'Wminus_HZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_WminusH125.root', c_ggH, '2e2mu'],
    'Wplus_HZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_WplusH125.root', c_ggH, '2e2mu'],
    'ZH_HZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ZH125.root', c_ggH, '2e2mu'],
    'ttH_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ttH125.root', c_ggH, '2e2mu'],
    'ZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ZZTo4lext2.root', c_qqZZ, '2e2mu'],
    'ZZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ZZZ.root', c_others, '2e2mu'],
    'WWZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_WWZ.root', c_others, '2e2mu'],
    'WZZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_WZZ.root', c_others, '2e2mu'],
    'ttW_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_TTWJetsToLNu.root', c_ttV, '2e2mu'],
    'ttZ_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_TTZToLLNuNu_M10.root', c_ttV, '2e2mu'],
    'ZX_2e2mu' : ['/eos/user/c/cmsdas/long-exercises/hh_bb4l/mvaNtuples_2bjet_DAS_alljet_mass_2018_fullmass_4lsel_fs2e2mu/reduced_ZXbkg_4ljjsel.root', c_ZX, '2e2mu']
}
