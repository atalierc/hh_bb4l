from ROOT import *

#import your dictionary
from bkg_2018 import *

def checkSumw2(h):
  if(h and h.GetDefaultSumw2()): 
      return h.Sumw2()

data = TH1D("", "", 120, 80, 200)
signal = TH1D("", "", 120, 80, 200)
frame = TH1D("", "", 120, 80, 200)
histo_bkg_all = TH1D("","", 120, 80, 200)

control_region = True
ratio = True

for name, sample_name in files.items():        
  if(sample_name[2] is '4mu'):
  #if(sample_name[2] is '2e2mu' or sample_name[2] is '4e' or sample_name[2] is '4mu'):
    inputFile=TFile(sample_name[0])
    tree=inputFile.Get('reducedTree')
    print sample_name[0]
    for entry in tree:
    #    if(abs(entry_signal.f_ZZmass - 125) <= 10 and entry_signal.f_Njets >= 2):
      sample_name[1].histo.Fill(entry.f_ZZmass, entry.f_weightsignal_nominal)

if(control_region is True):           
  lines = [line.rstrip('\n') for line in open('all_data_2018.txt')]
  for contents in lines:
    inputFile_data = TFile(contents)
    tree_data = inputFile_data.Get('reducedTree')
    print contents
    for entry_data in tree_data:
      #      if(abs(entry_data.f_ZZmass - 125) <= 10 and entry_data.f_Njets >= 2):
      data.Fill(entry_data.f_ZZmass)


lines = [line.rstrip('\n') for line in open('all_signal_2018.txt')]
for contents in lines:
  inputFile_signal = TFile(contents)
  tree_signal=inputFile_signal.Get('reducedTree')
  print contents
  for entry_signal in tree_signal:
    #    if(abs(entry_signal.f_ZZmass - 125) <= 10 and entry_signal.f_Njets >= 2):
    signal.Fill(entry_signal.f_ZZmass, entry_signal.f_weightsignal_nominal)
      

canvas = TCanvas("canvas", "canvas", 600,600,600,600)
canvas.cd()
canvas.SetLogy()

stack = THStack('stack','stack')
stack.Add(c_others.histo)
stack.Add(c_ZX.histo)
stack.Add(c_ttV.histo)
stack.Add(c_ggZZ.histo)
stack.Add(c_qqZZ.histo)
stack.Add(c_ggH.histo)

 
lista = TList()
lista.Add(c_others.histo)
lista.Add(c_ZX.histo)
lista.Add(c_ttV.histo)
lista.Add(c_ggH.histo)
lista.Add(c_ggZZ.histo)
lista.Add(c_qqZZ.histo)

histo_bkg_all.Merge(lista)

c_ZX.histo.Rebin(5)
c_others.histo.Rebin(5)
c_ttV.histo.Rebin(5)
c_ggH.histo.Rebin(5)
c_ggZZ.histo.Rebin(5)
c_qqZZ.histo.Rebin(5)
signal.Rebin(5)

print "ZX ", c_ZX.histo.Integral()
print "others ", c_others.histo.Integral()
print "ttV ", c_ttV.histo.Integral()
print "ggH ", c_ggH.histo.Integral()
print "qqZZ ", c_qqZZ.histo.Integral()
print "ggZZ ", c_ggZZ.histo.Integral()
print "signal ", signal.Integral()

histo_bkg_all.Rebin(5)

data.SetMarkerStyle(8)
data.SetMarkerColor(kBlack)
data.SetLineColor(kBlack)
data.Rebin(5)

#draw empty frame
frame.GetXaxis().SetTitle("mass_{ZZ} (GeV)")
frame.GetYaxis().SetTitleOffset(0.5)
frame.GetYaxis().SetTitle("Entries / 5 GeV")
frame.GetYaxis().SetTitleOffset(1.4)
frame.SetMaximum(1000000)
frame.SetMinimum(0.01)
frame.SetStats(0)

frame.Draw('HISTO')
stack.Draw("same HISTO")
data.Draw("EP same")

signal.Scale(100)
signal.SetLineStyle(7)
signal.SetLineWidth(2)
signal.SetLineColor(kRed)
signal.Draw("same")


ll = TPaveText(0.10, 0.91, 0.95, 0.99, "NDC")
ll.SetTextSize(0.024)
ll.SetTextFont(42)
ll.SetFillColor(0)
ll.SetBorderSize(0)
ll.SetMargin(0)
ll.SetTextAlign(12)
text = "CMS Preliminary"
ll.AddText(0.01,0.5,text)

text = "#sqrt{s} = 13 TeV, L = 59.7 fb^{-1}"
ll.AddText(0.67,0.6,text)

gPad.RedrawAxis()

legendmass4l = TLegend(0.47,0.65,0.67,0.89)
legendmass4l.SetTextSize(0.030)
legendmass4l.SetLineColor(0)
legendmass4l.SetLineWidth(1)
legendmass4l.SetFillColor(kWhite)
legendmass4l.SetBorderSize(0)
legendmass4l.AddEntry(c_ggZZ.histo,"gg#rightarrowZZ#rightarrow4l","f")
legendmass4l.AddEntry(c_qqZZ.histo,"qq#rightarrowZZ#rightarrow4l","f")
legendmass4l.AddEntry(c_ggH.histo,"gg#rightarrowH, qq#rightarrowH m_{H}= 125 GeV","f")
legendmass4l.AddEntry(c_ttV.histo,"ttV where V = Z, W","f")
legendmass4l.AddEntry(c_others.histo,"VVV where V = Z, W","f")
legendmass4l.AddEntry(c_ZX.histo, "Z+X", "f")
legendmass4l.AddEntry(data,"Data","lep")
legendmass4l.AddEntry(signal, "HH#rightarrowbb4l signal #times 100", "l")
legendmass4l.Draw("same")


if(ratio is True):
    frame2 = TH1D("","", 120, 80, 200)
    frame2.SetMaximum(2)
    frame2.SetMinimum(0)
    frame2.SetStats(0)
    frame2.GetYaxis().SetTitle("Data / mc")
    frame2.GetYaxis().SetNdivisions(508)
    h3 = data.Clone("h3")
    h3.Divide(histo_bkg_all)
    h3.SetMarkerStyle(8)
    h3.SetMarkerColor(kBlack)
    h3.SetLineColor(kBlack)
    canvasratio = 0.3
    canvas.SetBottomMargin(canvasratio + (1-canvasratio)*canvas.GetBottomMargin()-canvasratio*canvas.GetTopMargin())
    canvasratio = 0.16
    ratioPad =  TPad("pad2","pad2", 0,0,1,1)
    ratioPad.SetTopMargin((1-canvasratio) - (1-canvasratio)*ratioPad.GetBottomMargin()+canvasratio*ratioPad.GetTopMargin())
    ratioPad.SetFillStyle(4000)
    ratioPad.SetFillColor(4000)
    ratioPad.SetFrameFillColor(4000)
    ratioPad.SetFrameFillStyle(4000)
    ratioPad.SetFrameBorderMode(0)
    ratioPad.SetGrid(1,1)
    ratioPad.Draw("HISTO")
    ratioPad.cd()

    denRelUncH = TH1D()
    denRelUncH=histo_bkg_all.Clone("mcrelunc")
    
    GPoint=0
    denRelUnc=TGraphErrors(denRelUncH.GetXaxis().GetNbins())

    for xbin in range(0,(denRelUncH.GetXaxis().GetNbins()+1)):
        denRelUnc.SetPoint(GPoint, denRelUncH.GetBinCenter(xbin), 1.0)
        if(denRelUncH.GetBinContent(xbin)!=0):
            denRelUnc.SetPointError(GPoint,  denRelUncH.GetBinWidth(xbin)/2, denRelUncH.GetBinError(xbin)/denRelUncH.GetBinContent(xbin))
        else:
            denRelUnc.SetPointError(GPoint,  denRelUncH.GetBinWidth(xbin)/2, 0)
        GPoint=GPoint+1
        if(denRelUncH.GetBinContent(xbin) != 0):
            err=denRelUncH.GetBinError(xbin)/denRelUncH.GetBinContent(xbin)
            denRelUncH.SetBinContent(xbin,1)
            denRelUncH.SetBinError(xbin,err)
  
    gPad.Update()
    denRelUnc.Set(GPoint)
    denRelUnc.SetLineColor(1)
    denRelUnc.SetFillStyle(3005)
    denRelUnc.SetFillColor(kGray+3)
    denRelUnc.SetMarkerColor(1)
    denRelUnc.SetMarkerStyle(1)
    denRelUncH.Reset("")
    denRelUncH.SetTitle("")
    denRelUncH.SetStats(kFALSE)
    denRelUncH.Draw()
    denRelUnc.Draw("2 same")
    yscale = (1.0-0.2)/(0.2)
    denRelUncH.GetYaxis().SetNdivisions(505)
    denRelUncH.GetYaxis().SetTitle("Data/#Sigma Bkg.")
    denRelUncH.GetXaxis().SetTitle("")
    denRelUncH.SetMinimum(0)
    denRelUncH.SetMaximum(2)

    denRelUnc.GetXaxis().SetLabelFont(52)
    denRelUnc.GetXaxis().SetLabelOffset(0.007)
    denRelUnc.GetXaxis().SetLabelSize(0.03 * yscale)
    denRelUnc.GetXaxis().SetTitleFont(42)
    denRelUnc.GetXaxis().SetTitleSize(0.035 * yscale)
    denRelUnc.GetXaxis().SetTitleOffset(0.8)
    denRelUnc.GetYaxis().SetLabelFont(52)
    denRelUnc.GetYaxis().SetLabelOffset(0.007)
    denRelUnc.GetYaxis().SetLabelSize(0.03 * yscale)
    denRelUnc.GetYaxis().SetTitleFont(42)
    denRelUnc.GetYaxis().SetTitleSize(0.035 * yscale)
    denRelUnc.GetYaxis().SetTitleOffset(0.3)



    name = "CompHistogram"
    dataToObsH = data.Clone(name)
    checkSumw2(dataToObsH)
    dataToObsH.Sumw2()
    dataToObsH.Divide(histo_bkg_all)
    dataToObs = TGraphErrors(dataToObsH)
    dataToObs.SetMarkerColor(dataToObsH.GetMarkerColor())
    dataToObs.SetMarkerStyle(dataToObsH.GetMarkerStyle())
    dataToObs.SetMarkerSize(dataToObsH.GetMarkerSize())
    dataToObs.Draw("P same")
    

ll.Draw("same")


canvas.SaveAs('mass_ZZMass_2018.pdf')


